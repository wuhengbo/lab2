#include <linux/types.h>
#include <linux/vmalloc.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/ide.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include <linux/of_gpio.h>
#include <linux/semaphore.h>
#include <linux/timer.h>
#include <linux/i2c.h>
#include <linux/genhd.h>
#include <linux/blkdev.h>
#include <linux/hdreg.h>

#include <asm/uaccess.h>
#include <asm/io.h>

#define RAMDISK_SIZE	(50 * 1024 * 1024) 	/* Вместимость — 50 мб */
#define RAMDISK_NAME	"ramdisk"			/* им */
#define RADMISK_MINOR	 5					/* Это значит, что существует три сегмента диска! Не на третьем уровне! */

/* ramdisk Структура оборудования */
struct ramdisk_dev{
	int major;					/* Главное оборудование. */
	unsigned char *ramdiskbuf;	/* ramdisk Пространство памяти для моделирования блочных устройств */
	spinlock_t lock;			/* Спиновый замок. */
	struct gendisk *gendisk; 	/* gendisk */
	struct request_queue *queue;/* The request queue */

};

struct ramdisk_dev ramdisk;		/* ramdisk  equipment*/

/*
 * @description		: Open block device
 * @param - dev 	: block device
 * @param - mode 	: Open mode
 * @return 			: 0 success; Other failure
 */
int ramdisk_open(struct block_device *dev, fmode_t mode)
{
	printk("ramdisk open\r\n");
	return 0;
}

/*
 * @description		: Release block device
 * @param - disk 	: gendisk
 * @param - mode 	: Open model
 * @return 			: 0 success; Other failure
 */
void ramdisk_release(struct gendisk *disk, fmode_t mode)
{
	printk("ramdisk release\r\n");
}

/*
 * @description		: Obtaining Disk Information
 * @param - dev 	: block device
 * @param - geo 	: Open model
 * @return 			: 0 success; Other failure
 */
int ramdisk_getgeo(struct block_device *dev, struct hd_geometry *geo)
{
	/* This is relative to the concept of a mechanical hard disk */
	geo->heads = 16;			/* head */
	geo->cylinders = 64;	/* cylinder */
	geo->sectors = RAMDISK_SIZE / (16 * 64 *512); /* The number of sectors on a track */
	return 0;
}

/* 
 *  Block device manipulation function
 */
static struct block_device_operations ramdisk_fops =
{
	.owner	 = THIS_MODULE,
	.open	 = ramdisk_open,
	.release = ramdisk_release,
	.getgeo  = ramdisk_getgeo,
};

/*
 * @description	: Processing transmission
 * @param-req 	: request
 * @return 		: null
 */
static void ramdisk_transfer(struct request *req)
{	
	unsigned long start = blk_rq_pos(req) << 9;  	/* blk_rq_pos  get the sector address, shift it 9 bits to the byte address */
	unsigned long len  = blk_rq_cur_bytes(req);		/*   The size of the */

	/* Data buffer in BIO
	 * Read: Data read from disk is stored in buffer
	 * Write: buffer holds the data to be written to disk
	 */
	void *buffer = bio_data(req->bio);		
	
	if(rq_data_dir(req) == READ) 		
		memcpy(buffer, ramdisk.ramdiskbuf + start, len);
	else if(rq_data_dir(req) == WRITE) 
		memcpy(ramdisk.ramdiskbuf + start, buffer, len);

}

/*
 * @description	: Request handler function
 * @param-q 	: The request queue
 * @return 		: null
 */
void ramdisk_request_fn(struct request_queue *q)
{
	int err = 0;
	struct request *req;

	/* Loop through each request in the request queue */
	req = blk_fetch_request(q);
	while(req != NULL) {

		/* Do specific transfer processing for the request */
		ramdisk_transfer(req);

		/* Determine if it is the last request and get the next request if not
		 * Loop through all requests in the request queue。
		 */
		if (!__blk_end_request_cur(req, err))
			req = blk_fetch_request(q);
	}
}


/*
 * @description	: Drive exit function
 * @param 		: null
 * @return 		: null
 */
static int __init ramdisk_init(void)
{
	int ret = 0;
	printk("ramdisk init\r\n");

	/* 1、Apply for ramdisk memory */
	ramdisk.ramdiskbuf = (char *)vmalloc(RAMDISK_SIZE);
	if(ramdisk.ramdiskbuf == NULL) {
		ret = -EINVAL;
		goto ram_fail;
	}

	/* 2、Initialize the spin lock*/
	spin_lock_init(&ramdisk.lock);

	/* 3、Registering a Block device */
	ramdisk.major = register_blkdev(0, RAMDISK_NAME); /* The system automatically assigns the main device number */
	if(ramdisk.major < 0) {
		goto register_blkdev_fail;
	}  
	printk("ramdisk major = %d\r\n", ramdisk.major);

	/* 4、Allocates and initializes gendisk */
	ramdisk.gendisk = alloc_disk(RADMISK_MINOR);
	if(!ramdisk.gendisk) {
		ret = -EINVAL;
		goto gendisk_alloc_fail;
	}

	/* 5、Allocates and initializes the request queue */
	ramdisk.queue = blk_init_queue(ramdisk_request_fn, &ramdisk.lock);
	if(!ramdisk.queue) {
		ret = EINVAL;
		goto blk_init_fail;
	}

	/* 6、Add (register) disks */
	ramdisk.gendisk->major = ramdisk.major;		/* The main equipment,*/
	ramdisk.gendisk->first_minor = 0;			/* The first equipment number (the first equipment number) */
	ramdisk.gendisk->fops = &ramdisk_fops; 		/* Operation function */
	ramdisk.gendisk->private_data = &ramdisk;	/* Private data */
	ramdisk.gendisk->queue = ramdisk.queue;		/* The request queue */
	sprintf(ramdisk.gendisk->disk_name, RAMDISK_NAME); /* name */
	set_capacity(ramdisk.gendisk, RAMDISK_SIZE/512);	/* Device capacity (sector) */
	add_disk(ramdisk.gendisk);

	return 0;

blk_init_fail:
	put_disk(ramdisk.gendisk);
	//del_gendisk(ramdisk.gendisk);
gendisk_alloc_fail:
	unregister_blkdev(ramdisk.major, RAMDISK_NAME);
register_blkdev_fail:
	vfree(ramdisk.ramdiskbuf); /* Free memory */
ram_fail:
	return ret;
}

/*
 * @description	: Drive exit function
 * @param 		: null
 * @return 		: null
 */
static void __exit ramdisk_exit(void)
{
	printk("ramdisk exit\r\n");
	/* Release gendisk */
	del_gendisk(ramdisk.gendisk);
	put_disk(ramdisk.gendisk);

	/* Clear the request queue */
	blk_cleanup_queue(ramdisk.queue);

	/* Deregistering block devices */
	unregister_blkdev(ramdisk.major, RAMDISK_NAME);

	/* Free memory */
	vfree(ramdisk.ramdiskbuf); 
}

module_init(ramdisk_init);
module_exit(ramdisk_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("caixukun");

